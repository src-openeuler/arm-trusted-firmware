%global debug_package %{nil}

Name:          arm-trusted-firmware
Version:       2.12.0
Release:       1
Summary:       ARM Trusted Firmware
License:       BSD
URL:           https://github.com/ARM-software/arm-trusted-firmware/wiki
Source0:       https://github.com/ARM-software/arm-trusted-firmware/archive/v%{version}/%{name}-%{version}.tar.gz
ExclusiveArch: aarch64
BuildRequires: dtc
BuildRequires: gcc openssl-devel

%description
Trusted Firmware-A is a reference implementation of secure world software
for Arm A-Profile architectures (Armv8-A and Armv7-A), including an Exception Level 3 (EL3) Secure Monitor.

%package       -n arm-trusted-firmware-armv8
Summary:       ARMv8-A Trusted Firmware
%description   -n arm-trusted-firmware-armv8
ARM Trusted Firmware for various ARMv8-A SoCs.


%prep
%autosetup -p1 -n %{name}-%{version}
sed -i 's/arm-none-eabi-/arm-linux-gnu-/' plat/rockchip/rk3399/drivers/m0/Makefile

%build
export CC=gcc
for soc in hikey hikey960 imx8qm imx8qx juno rk3368 rk3328 rpi3 sun50i_a64 sun50i_h6 zynqmp
do
make HOSTCC="%{CC} $RPM_OPT_FLAGS -fPIE -Wl,-z,relro,-z,now" CROSS_COMPILE="" PLAT=$(echo $soc) bl31
done


%install
install -d %{buildroot}%{_datadir}/%{name}
for soc in hikey hikey960 imx8qm imx8qx juno rpi3 sun50i_a64 sun50i_h6 zynqmp
do
install -d %{buildroot}%{_datadir}/%{name}/$(echo $soc)/
if [ -f build/$(echo $soc)/release/bl31.bin ]; then
install -p -m 0644 build/$(echo $soc)/release/bl31.bin /%{buildroot}%{_datadir}/%{name}/$(echo $soc)/
fi
done

for soc in rk3368 rk3328
do
install -d %{buildroot}%{_datadir}/%{name}/$(echo $soc)/
for file in bl31/bl31.elf m0/rk3399m0.bin m0/rk3399m0.elf
do
 if [ -f build/$(echo $soc)/release/$(echo $file) ]; then
   install -p -m 0644 build/$(echo $soc)/release/$(echo $file) /%{buildroot}%{_datadir}/%{name}/$(echo $soc)/
 fi
done
done

strip %{buildroot}/%{_datadir}/%{name}/rk3328/bl31.elf
strip %{buildroot}/%{_datadir}/%{name}/rk3368/bl31.elf

%files -n arm-trusted-firmware-armv8
%license license.rst
%doc readme.rst
%{_datadir}/%{name}

%changelog
* Wed Nov 27 2024 yaoxin <yao_xin001@hoperun.com> - 2.12.0-1
- Update to 2.12.0
- Bootloader Images:
    * remove unused plat_try_next_boot_source
- Architecture:
    *Branch Record Buffer Extension (FEAT_BRBE)
    * allow RME builds with BRBE
- Arm:
    * avoid stripping kernel trampoline
    * add DRAM memory regions that linux kernel can share
    * add optee specific mem-size attribute
    * add secure uart interrupt in device region
    * enable FEAT_MTE2
    * fix the FF-A optee manifest by adding the boot info node
    * update the memory size allocated to optee at EL1
- Intel:
    * add cache invalidation during BL31 initialization
    * add in JTAG ID for Linux FCS
    * add in missing ECC register
    * add in watchdog for QSPI driver
    * bridge ack timing issue causing fpga config hung
    * correct macro naming
    * f2sdram bridge quick write thru failed
    * fix bridge enable and disable function
    * fix CCU for cache maintenance
    * flush L1/L2/L3/Sys cache before HPS cold reset
    * implement soc and lwsoc bridge control for burst speed
    * refactor SDMMC driver for Altera products
    * remove redundant BIT_32 macro
    * software workaround for bridge timeout
    * update Agilex5 BL2 init flow and other misc changes
    * update Agilex5 warm reset subroutines
    * update all the platforms hand-off data offset value
    * update CCU configuration for Agilex5 platform
    * update mailbox SDM printout message
    * update memcpy to memcpy_s ([e264b55]
    * update outdated code for Linux direct boot
    * update preloaded_bl33_base for legacy product
    * update sip smc config addr for agilex5
    * update the size with addition 0x8000 0000 base

* Tue Oct 15 2024 yaoxin <yao_xin001@hoperun.com> - 2.9-4
- Fix CVE-2024-6287

* Tue Jul 09 2024 zhangxianting <zhangxianting@uniontech.com> - 2.9-3
- Fix CVE-2024-6563 CVE-2024-6564

* Tue Jan 23 2024 yaoxin <yao_xin001@hoperun.com> - 2.9-2
- Fix CVE-2023-49100

* Fri Jul 07 2023 xu_ping <707078654@qq.com> -2.9-1
- Upgrade to 2.9

* Wed Dec 07 2022 yaoxin <yaoxin30@h-partners.com> -2.3-2
- Add RELRO,PIE,BIND_NOW flags and fix not striped problem

* Tue Jan 5 2021 huanghaitao <huanghaitao8@huawei.com> - 2.3-1
- Update to 2.3 release

* Wed Sep 16 2020 wangyue <wangyue92@huawei.com> - 1.6-2
- fix CVE-2017-15031

* Tue Dec 31 2019 Shuaishuai Song <songshuaishuai2@huawei.com> 1.6-0.2
- package init
